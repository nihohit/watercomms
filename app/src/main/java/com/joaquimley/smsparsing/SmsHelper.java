package com.joaquimley.smsparsing;

import android.content.Context;
import android.net.UrlQuerySanitizer;
import android.os.AsyncTask;
import android.telephony.SmsManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

import static android.os.AsyncTask.THREAD_POOL_EXECUTOR;

/**
 * Constants helper class
 */

class AsyncTaskRunnable extends AsyncTask<Void, Void, Void> {
    private static final String queryBase = "https://dny54p7e54.execute-api.eu-central-1.amazonaws.com/default/smsHandler?smsSender=%s&smsBody=%s";
    private static RequestQueue queue;
    private final String phoneNumber, messageBody;

    public AsyncTaskRunnable(String phoneNumber, String messageBody) {
        this.phoneNumber = phoneNumber;
        this.messageBody = messageBody;
    }

    private static final String TAG = "SmsHelper";

    @Override
    protected Void doInBackground(Void... voids) {
        String url = null;
        try {
            url = String.format(queryBase, this.phoneNumber, URLEncoder.encode(this.messageBody, "utf-8"));
        } catch (Exception exception) {
            Log.d(TAG, "Received exception: " + exception);
            return null;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject json = new JSONObject(response);
                            SmsHelper.sendDebugSms(json.get("sendTo").toString(), json.get("message").toString());
                            Log.d(TAG, "Send SMS with content " + json.get("message") + " to number " + json.get("sendTo"));
                        } catch (Exception exception) {
                            Log.d(TAG, "Received exception: " + exception);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "Received error: " + error);
                    }
                });

// Add the request to the RequestQueue.
        queue.add(stringRequest);
        return null;
    }

    public static void SetupQueue(Context context) {
        queue = Volley.newRequestQueue(context);
    }
}

public class SmsHelper {
    public static final String SMS_CONDITION = "Some condition";

    public static boolean isValidPhoneNumber(String phoneNumber) {
        return android.util.Patterns.PHONE.matcher(phoneNumber).matches();
    }

    public static void SetupContext(Context context) {
        AsyncTaskRunnable.SetupQueue(context);
    }

    public static void sendDebugSms(String number, String smsBody) {
        // TODO - hack that allows numbers in israel to work.
        number = number.replaceFirst("972","0").replaceAll("\\s","");
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(number, null, smsBody, null, null);
    }

    public static void sendLambdaCall(String number, String messageBody) {
        new AsyncTaskRunnable(number, messageBody).executeOnExecutor(THREAD_POOL_EXECUTOR);
    }
}
